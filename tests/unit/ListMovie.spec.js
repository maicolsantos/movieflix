import { mount } from '@vue/test-utils'

import ListMovie from '@/components/ListMovie'

describe('ListMovie', () => {
  it('renders component when passed', () => {
    const wrapper = mount(ListMovie)

    const listMovie = wrapper.find(ListMovie)
    expect(listMovie.is(ListMovie)).toBe(true)

    const div = wrapper.find('div')
    expect(div.is('div')).toBe(true)

    const listMovieName = wrapper.find({ name: 'list-movie' })
    expect(listMovieName.is(ListMovie)).toBe(true)
  })
})
