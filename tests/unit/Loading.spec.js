import { mount } from '@vue/test-utils'

import Loading from '@/components/Loading'

/* eslint-disable */
const html = '<div class=\"loading\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"30px\" height=\"30px\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\"><circle cx=\"50\" cy=\"50\" fill=\"none\" r=\"40\" stroke=\"#00695C\" strokeWidth=\"8\" strokeLinecap=\"round\" transform=\"rotate(45.859 50 50)\"><animateTransform attributeName=\"transform\" type=\"rotate\" calcMode=\"linear\" values=\"0 50 50;180 50 50;720 50 50\" keyTimes=\"0;0.5;1\" dur=\"1.7s\" begin=\"0s\" repeatCount=\"indefinite\"></animateTransform><animate attributeName=\"stroke-dasharray\" calcMode=\"linear\" values=\"25.132741228718345 226.1946710584651;226.1946710584651 25.132741228718345;25.132741228718345 226.1946710584651\" keyTimes=\"0;0.5;1\" dur=\"1.7s\" begin=\"0s\" repeatCount=\"indefinite\"></animate></circle></svg> <p>Carregando...</p></div>'
/* eslint-enable */

describe('Loading', () => {
  it('renders component when passed', () => {
    const wrapper = mount(Loading)
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.find('li').exists()).toBe(false)
    expect(wrapper.findAll('div').exists()).toBe(true)
    expect(wrapper.findAll('li').exists()).toBe(false)
    expect(wrapper.html()).toBe(html)
  })
})
