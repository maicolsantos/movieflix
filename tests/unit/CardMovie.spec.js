import { shallowMount } from '@vue/test-utils'

import CardMovie from '@/components/CardMovie'

describe('CardMovie', () => {
  it('renders props.movie when passed', () => {
    const movie = { id: 1 }

    const wrapper = shallowMount(CardMovie, {
      propsData: { movie },
    })

    wrapper.setData({ movie })
    expect(wrapper.vm.movie.id).toBe(1)
  })
})
