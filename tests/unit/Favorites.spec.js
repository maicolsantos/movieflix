import { shallowMount } from '@vue/test-utils'

import Favorites from '@/components/Favorites'

describe('Favorites', () => {
  it('renders props movie and isFavorite when passed', () => {
    const movie = { id: 1 }
    const isFavorite = true

    const wrapper = shallowMount(Favorites, {
      propsData: { movie, isFavorite },
    })

    wrapper.setData({ movie, isFavorite })
    expect(wrapper.vm.movie.id).toBe(1)
    expect(wrapper.vm.isFavorite).toBe(true)
  })
})
