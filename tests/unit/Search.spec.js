import { mount } from '@vue/test-utils'

import Search from '@/components/Search'

describe('Search', () => {
  it('renders component when passed', () => {
    const wrapper = mount(Search)

    const search = wrapper.find(Search)
    expect(search.is(Search)).toBe(true)

    const form = wrapper.find('form')
    expect(form.is('form')).toBe(true)

    const input = wrapper.find('input')
    expect(input.is('input')).toBe(true)

    const button = wrapper.find('button')
    expect(button.is('button')).toBe(true)

    const searchName = wrapper.find({ name: 'Search' })
    expect(searchName.is(Search)).toBe(true)
  })
})
