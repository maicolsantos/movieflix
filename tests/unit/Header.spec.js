import Vuex from 'vuex'
import { createLocalVue, mount } from '@vue/test-utils'

import store from '@/store'
import Header from '@/components/Header'

describe('Header', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)

  it('renders component when passed', () => {
    const wrapper = mount(Header, {
      store,
      localVue
    })

    const header = wrapper.find(Header)
    expect(header.is(Header)).toBe(true)

    const headerTag = wrapper.find('header')
    expect(headerTag.is('header')).toBe(true)

    const nav = wrapper.find('nav')
    expect(nav.is('nav')).toBe(true)

    const li = wrapper.find('li')
    expect(li.is('li')).toBe(true)
  })
})
