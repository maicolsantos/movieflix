import axios from 'axios'

const apiKey = 'c5718e9c00a4f235330115ef539146a6'
const lang = '&language=pt-BR'

const api = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
})

const fetchMovies = page => (
  api.get(`movie/now_playing?api_key=${apiKey}${lang}&page=${page}`)
)
const fetchMovieById = id => api.get(`movie/${id}?api_key=${apiKey}${lang}`)
const searchMovie = term => (
  api.get(`search/movie?api_key=${apiKey}&query=${term}${lang}`)
)

export {
  fetchMovies,
  fetchMovieById,
  searchMovie,
}
