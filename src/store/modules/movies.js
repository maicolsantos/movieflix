import firebase from 'firebase'

import store from '@/store'
import * as types from '@/store/mutation-types'
import {
  fetchMovies,
  fetchMovieById,
  searchMovie,
} from '@/services/api'

const time = 1000

const firebaseInterceptor = (callback, dispatch) => (
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (user.emailVerified) {
        callback()
      } else {
        dispatch()
      }
    }
  })
)

const initialState = {
  movies: [],
  movie: {},
  favorites: [],
  loading: false,
}

const getters = {
  movies: state => state.movies,
  movie: state => state.movie,
  favorites: state => state.favorites,
  loading: state => state.loading,
}

const actions = {
  getMovies: async ({ commit, dispatch }, page) => {
    const { user } = store.getters
    commit(types.SET_LOADING, true)

    if (user && user.id) {
      dispatch('getFavorites')
    }

    try {
      const response = await fetchMovies(page)
      commit(types.SET_MOVIES, response.data.results)
    } catch (error) {
      console.error(error)
    } finally {
      setTimeout(() => commit(types.SET_LOADING, false), time)
    }
  },
  getMovieById: async ({ commit, dispatch }, id) => {
    const { user } = store.getters
    commit(types.SET_LOADING, true)

    if (user && user.id) {
      dispatch('getFavorites')
    }

    try {
      const response = await fetchMovieById(id)
      commit(types.SET_MOVIE, response.data)
    } catch (error) {
      console.error(error)
    } finally {
      setTimeout(() => commit(types.SET_LOADING, false), time)
    }
  },
  getSearchMovie: async ({ commit }, term) => {
    commit(types.SET_LOADING, true)

    try {
      const response = await searchMovie(term)
      commit(types.SET_MOVIES, response.data)
    } catch (error) {
      console.error(error)
    } finally {
      setTimeout(() => commit(types.SET_LOADING, false), time)
    }
  },
  getFavorites: ({ commit }, data = null) => {
    const { user } = store.getters

    if (user && user.id) {
      firebase.database().ref(user.id).on('value', (snap) => {
        commit(types.SET_FAVORITES, snap.val() || {})
      })
    } else {
      commit(types.SET_FAVORITES, data)
    }
  },
  addToFavorites: ({ dispatch }, movie) => {
    const { user } = store.getters

    firebaseInterceptor(
      () => firebase.database().ref(`${user.id}/${movie.id}`).set({ ...movie }),
      () => dispatch('logout')
    )
  },
  removeFavorites: ({ dispatch }, movieId) => {
    const { user } = store.getters

    firebaseInterceptor(
      () => firebase.database().ref(`${user.id}/${movieId}`).remove(),
      () => dispatch('logout')
    )
  },
}

const mutations = {
  [types.SET_MOVIES](state = initialState, data) {
    state.movies = [
      ...state.movies,
      ...data,
    ]
  },
  [types.SET_MOVIE](state = initialState, data) {
    state.movie = data
  },
  [types.SET_FAVORITES](state = initialState, data) {
    state.favorites = Object.keys(data).map(item => data[item]) || []
  },
  [types.SET_LOADING](state = initialState, data) {
    state.loading = data
  },
}

export default {
  state: initialState,
  getters,
  actions,
  mutations
}
