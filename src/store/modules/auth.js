import firebase from 'firebase'

import * as types from '@/store/mutation-types'

const storageName = 'movieflix/user'
const user = JSON.parse(localStorage.getItem(storageName))

const initialState = {
  ...user || {},
  isLogged: !!user,
}

const getters = {
  user: state => state.user,
  isLogged: state => state.isLogged,
}

const actions = {
  login: async ({ commit, dispatch }) => {
    try {
      const provider = new firebase.auth.GoogleAuthProvider()
      const result = await firebase.auth().signInWithPopup(provider)
      localStorage.setItem(storageName, JSON.stringify({
        user: result.additionalUserInfo.profile
      }))
      commit(types.SET_USER, result.additionalUserInfo.profile)
      commit(types.IS_LOGGED, true)
      dispatch('getFavorites')
    } catch (error) {
      console.error(error)
      commit(types.IS_LOGGED, false)
    }
  },
  logout: async ({ commit, dispatch }) => {
    await firebase.auth().signOut()
    localStorage.removeItem(storageName)
    commit(types.SET_USER, {})
    commit(types.IS_LOGGED, false)
    dispatch('getFavorites', {})
  }
}

const mutations = {
  [types.SET_USER](state = initialState, data) {
    state.user = data
  },
  [types.IS_LOGGED](state = initialState, data) {
    state.isLogged = data
  },
}

export default {
  state: initialState,
  getters,
  actions,
  mutations
}
