import Vue from 'vue'
import firebase from 'firebase'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import App from './App.vue'
import router from './router'
import store from '@/store'
import '@/filters'

Vue.config.productionTip = false
Vue.component('v-icon', Icon)

let app = ''
const config = {
  apiKey: 'AIzaSyABhEe5LvA9wYB8KdliU_YPfGVkG2ul9YU',
  authDomain: 'movieflix-sas.firebaseapp.com',
  databaseURL: 'https://movieflix-sas.firebaseio.com',
  projectId: 'movieflix-sas',
  storageBucket: 'movieflix-sas.appspot.com',
  messagingSenderId: '34368774028',
}

firebase.initializeApp(config)

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  }
})
