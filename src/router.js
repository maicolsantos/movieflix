import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Single from './views/Single'
import Favorites from './views/Favorites'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/movie/:id',
      name: 'single',
      component: Single,
    },
    {
      path: '/my-favorites',
      name: 'my-favorites',
      component: Favorites,
    },
  ],
})
