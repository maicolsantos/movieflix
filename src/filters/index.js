import Vue from 'vue'
import moment from 'moment'

const yearFormat = date => (
  moment(date).format('YYYY')
)

Vue.filter('yearFormat', date => yearFormat(date))
